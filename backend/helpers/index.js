const axios = require("axios");

const getCharacters = async () => {
  const fechtCharacters = await axios.get("https://api.got.show/api/show/characters");
  return fechtCharacters.data;
};

module.exports = { getCharacters };
