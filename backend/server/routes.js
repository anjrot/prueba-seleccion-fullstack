const express = require("express");
const router = express.Router();

const CharacterController = require("../controllers/characters");

router.get("/api/characters/:id", CharacterController.getCharacterById);
router.get("/api/characters", CharacterController.getCharacters);

module.exports = router;
