# Frontend para la prueba de seleccion fullstack de Tactech

## Modo de instalación.

- Clonar este repo.
- Para ejecutarlo en local se debe tener instalado y corriendo mongoDb.
- Luego en una terminal ejecutar

```bash
  $ cd backend
  $ npm install
```

```bash
  $ npm start
```

Al correr por primera vez la app, esta buscará y guardará los personajes de la api en el siguiente endpoint:
`https://api.got.show/api/show/characters`

## Endpoints de la app
`GET /api/characters` Obtiene todos los personajes en bd
`GET /api/characters:id` Obtiene un personaje por su id


## Herramientas utilizadas:

- [Node v14.3.0](https://nodejs.org/)
- [Express v4.17.1](https://expressjs.com/es/)
- [Mongoose v5.9.27](https://mongoosejs.com/)

Gracias por la oportunidad!

Enjoy it 🚀😎

Antonio Rodríguez
FullStack Developer.
