const mongoose = require("mongoose");
const paginate = require("mongoose-paginate-v2");

const CharacterSchema = mongoose.Schema({
  name: {
    type: String
  },
  gender: {
    type: String
  },
  titles: {
    type: Array
  },
  image: {
    type: String
  },
  slug: {
    type: String
  },
  pagerank: {
    type: Object
  },
  age: {
    type: Object
  },
  house: {
    type: Object
  }
});

CharacterSchema.plugin(paginate);

module.exports = mongoose.model("characters", CharacterSchema);
