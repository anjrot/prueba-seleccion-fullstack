const CharacterModel = require("../models/Characters");

const getCharacters = async (req, resp) => {
  const page = Number(req.query.page) || 1;
  const name = req.query.name || "";
  const house = req.query.house || "";
  try {
    const characters = await CharacterModel.paginate(
      {
        name: { $regex: new RegExp(name, "i") },
        house: { $regex: house }
      },
      { page }
    );
    const { docs, nextPage, prevPage } = characters;
    resp.send({
      docs,
      nextPage,
      prevPage
    });
  } catch (error) {
    console.log("error :>> ", error);
  }
};

const getCharacterById = async (req, res) => {
  const { id } = req.params;
  try {
    const character = await CharacterModel.findOne({ _id: id });
    res.status(200).send(character);
  } catch (error) {
    console.log("error :>> ", error);
  }
};

module.exports = { getCharacters, getCharacterById };
