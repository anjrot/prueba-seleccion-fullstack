const app = require("./server/app");
const { getAndSaveCharacters } = require("./helpers");
const { dbConnect } = require("./config/db");

const port = process.env.PORT || 3000;

(async () => {
  await dbConnect();

  app.listen(port, () => console.log(`Servidor conectado en el puerto ${port}`));
})();
