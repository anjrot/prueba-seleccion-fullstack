require("dotenv").config();
const mongoose = require("mongoose");
const CharacterModel = require("../models/Characters");
const { getCharacters } = require("../helpers");

const dbConnect = async () => {
  try {
    mongoose.set("useCreateIndex", true);
    await mongoose.connect(process.env.DB_MONGO, {
      useNewUrlParser: true,
      useUnifiedTopology: true,
      useFindAndModify: false
    });
    console.log(`Db conectada en ${process.env.DB_MONGO}`);
  } catch (error) {
    console.log(error);
    process.exit(1);
  }
};

const connection = mongoose.connection;
connection.once("open", () => {
  connection.db.listCollections().toArray(async (err, names) => {
    if (err) {
      console.log("err :>> ", err);
    } else {
      const collectionExists = names.find(name => name.name === "characters");
      // if collection doesn't exists will bring and save all Characters, else return!!
      if (collectionExists !== undefined) return;
      try {
        const fetchCharacters = await getCharacters();
        fetchCharacters.forEach(async character => {
          const saveCharacter = new CharacterModel(character);
          await saveCharacter.save();
        });
      } catch (error) {
        console.log(error);
        process.exit(1);
      }
    }
  });
});

module.exports = { dbConnect };
