import axios from "axios";
import Layout from "../../Components/layout/layout";
import CharacterContent from "../../Components/layout/characterContent";
import { Container, Wrapper } from "../../Components/layout/container";

const Character = ({ data }) => {
  return (
    <Layout>
      <Container>
        <Wrapper>{Object.keys(data).length !== 0 ? <CharacterContent content={data} /> : null}</Wrapper>
      </Container>
    </Layout>
  );
};

export async function getServerSideProps(context) {
  const { id } = context.params;
  const fetchCharacter = await axios.get(`http://localhost:4000/api/characters/${id}`);
  const data = fetchCharacter.data;
  return { props: { data } };
}

export default Character;
