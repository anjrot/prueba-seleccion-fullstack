import "../styles/global.css";
import Head from "next/head";
import { Provider } from "react-redux";
import { createWrapper } from "next-redux-wrapper";
import store from "../redux/store";
import { ThemeProvider } from "@material-ui/core/styles";
import theme from "../styles/theme";

const App = ({ Component, pageProps }) => {
  return (
    <Provider store={store}>
      <Head>
        <title>FrontEnd para la prueba de seleccion fullstack de Tactech</title>
        <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700&display=swap" />
      </Head>
      <ThemeProvider theme={theme}>
        <Component {...pageProps} />
      </ThemeProvider>
    </Provider>
  );
};

const makeStore = () => store;
const wrapper = createWrapper(makeStore);
export default wrapper.withRedux(App);
