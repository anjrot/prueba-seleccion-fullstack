import Layout from "../Components/layout/layout";
import { Container, Wrapper } from "../Components/layout/container";
import ListConten from "../Components/layout/listContent";
const Home = () => {
  return (
    <Layout>
      <Container>
        <Wrapper>
          <ListConten />
        </Wrapper>
      </Container>
    </Layout>
  );
};

export default Home;
