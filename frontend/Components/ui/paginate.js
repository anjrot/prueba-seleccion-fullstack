import { useDispatch, useSelector } from "react-redux";
import { IconButton, InputBase } from "@material-ui/core";
import { DividedContent } from "../layout/container";
import { ArrowBack, ArrowForward } from "@material-ui/icons";
import { getCharacters } from "../../redux/actions/charactersAction";
import Search from "./search";
const Paginate = () => {
  const { payload } = useSelector(state => state.characters);

  const dispatch = useDispatch();
  const handlePrev = prevPage => {
    dispatch(getCharacters({ page: prevPage }));
  };
  const handleNext = nextPage => {
    dispatch(getCharacters({ page: nextPage }));
  };
  return (
    <DividedContent>
      <div>
        <IconButton
          color="primary"
          onClick={payload.prevPage !== null ? () => handlePrev(payload.prevPage) : null}
          disabled={payload.prevPage !== null ? false : true}
        >
          <ArrowBack fontSize="large" />
        </IconButton>
        <IconButton
          color="primary"
          onClick={payload.nextPage !== null ? () => handleNext(payload.nextPage) : null}
          disabled={payload.nextPage !== null ? false : true}
        >
          <ArrowForward fontSize="large" />
        </IconButton>
      </div>
      <Search />
    </DividedContent>
  );
};

export default Paginate;
