import { useState } from "react";
import { useDispatch } from "react-redux";
import { getCharacters } from "../../redux/actions/charactersAction";
import { useStyles } from "../../hooks/useStyles";
import { InputBase } from "@material-ui/core";
import SearchIcon from "@material-ui/icons/Search";

const Search = () => {
  const [search, setSearch] = useState({ value: "" });
  const dispatch = useDispatch();
  const classes = useStyles();

  const { value } = search;

  const handleSearch = e => {
    setSearch({
      ...search,
      value: e.target.value
    });

    dispatch(getCharacters({ name: value }));
  };

  return (
    <div className={classes.search}>
      <div className={classes.searchIcon}>
        <SearchIcon />
      </div>
      <InputBase
        placeholder="Buscar…"
        classes={{
          root: classes.inputRoot,
          input: classes.inputInput
        }}
        inputProps={{ "aria-label": "buscar" }}
        onChange={handleSearch}
        value={value}
      />
    </div>
  );
};

export default Search;
