import { useSelector } from "react-redux";
import { Grid } from "@material-ui/core";
import Paginate from "../ui/paginate";
import Content from "./content";
const ListConten = () => {
  const { payload } = useSelector(state => state.characters);

  return (
    <>
      {payload ? (
        <>
          <Paginate />
          <Grid container justify="center" spacing={2}>
            {payload.docs.map(item => {
              return (
                <Grid item xs={4} key={item._id}>
                  <Content content={item} />
                </Grid>
              );
            })}
          </Grid>
        </>
      ) : (
        <h1>No hay datos!!</h1>
      )}
    </>
  );
};

export default ListConten;
