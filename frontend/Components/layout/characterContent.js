import { Grid } from "@material-ui/core";
import { ImageCard } from "./container";

const CharacterContent = ({ content }) => {
  const { name, age, gender, house, image, pagerank, titles } = content;
  return (
    <>
      {content ? (
        <Grid container justify="center" spacing={2}>
          <Grid item xs={6}>
            <ImageCard auto rounded src={image} />
          </Grid>
          <Grid item xs={6}>
            <h1>{name}</h1>
            <ul className="none">
              <li>Edad: {age.age}</li>
              <li>Sexo: {gender}</li>
              <li>Casa: {house}</li>
              <li>Rank: {pagerank.rank}</li>
              <li>
                Apodos:
                {titles.length !== 0 ? (
                  <ul>
                    {titles.map((title, index) => (
                      <li key={`${title}_${index}`}>{title}</li>
                    ))}
                  </ul>
                ) : (
                  " Sin apodos"
                )}
              </li>
            </ul>
          </Grid>
        </Grid>
      ) : null}
    </>
  );
};

export default CharacterContent;
