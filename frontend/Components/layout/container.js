import styled from "@emotion/styled";

export const Container = styled.div`
  width: 100%;
`;

export const Wrapper = styled.div`
  width: 85%;
  display: block;
  margin: 2em auto;
`;

export const ImageCard = styled.img`
  width: 100%;
  height: ${props => (props.auto ? "auto" : "400px")};
  border-radius: ${props => (props.rounded ? "15px" : "none")};
  object-fit: cover;
  object-position: top;
`;

export const DividedContent = styled.div`
  display: flex;
  justify-content: space-between;
`;

export const Logo = styled.a`
  color: #fff;
  text-decoration: none;
  font-size: 1.5em;
  cursor: pointer;
  &:hover {
    opacity: 0.8;
  }
`;

export const CustomButton = styled.a`
  display: block;
  margin: auto;
  padding: 5px 15px;
  background: #4050b5;
  color: #fff;
  text-decoration: none;
  border-radius: 10px;
  font-size: 0.7em;
  cursor: pointer;
  &:hover {
    opacity: 0.8;
  }
`;
