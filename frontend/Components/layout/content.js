import Link from "next/link";
import { Card, CardMedia, CardHeader, CardActions } from "@material-ui/core";
import { ImageCard, CustomButton } from "./container";

const Content = ({ content }) => {
  return (
    <Card>
      <CardHeader title={content.name} subheader={`House: ${content.house}`} />
      <CardMedia>
        <ImageCard src={content.image} alt="ramdon" />
      </CardMedia>
      <CardActions>
        <Link href="/character/[id]" as={`/character/${content._id}`}>
          <CustomButton>Ver personaje</CustomButton>
        </Link>
      </CardActions>
    </Card>
  );
};

export default Content;
