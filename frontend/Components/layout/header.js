import Link from "next/link";
import { AppBar } from "@material-ui/core";
import { Wrapper, Logo } from "../layout/container";

const Header = () => {
  return (
    <AppBar position="static">
      <Wrapper>
        <Link href="/">
          <Logo>Prueba Tactech </Logo>
        </Link>
      </Wrapper>
    </AppBar>
  );
};

export default Header;
