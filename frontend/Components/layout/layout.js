import { useEffect } from "react";
import { useDispatch } from "react-redux";
import { getCharacters } from "../../redux/actions/charactersAction";
import { Paper } from "@material-ui/core";
import Header from "./header";

const Layout = ({ children }) => {
  const dispatch = useDispatch();
  useEffect(() => {
    dispatch(getCharacters());
  }, []);
  return (
    <Paper elevation={0}>
      <Header />
      <div>{children}</div>
    </Paper>
  );
};

export default Layout;
