import { createMuiTheme } from "@material-ui/core/styles";

const theme = createMuiTheme({
  palette: {
    type: "light"
  }
  //   overrides: {
  //     MuiTextField: {
  //       root: {
  //         flexGrow: 1,
  //         margin: "10px"
  //       }
  //     },
  //     MuiInputLabel: {
  //       root: {
  //         "&$focused": {
  //           // color: "#ffffff"
  //           // fontSize: "25px"
  //         }
  //       }
  //     },
  //     MuiFormControl: {
  //       root: {
  //         flexGrow: 1,
  //         margin: "10px"
  //       }
  //     },
  //     MuiButton: {
  //       root: {
  //         display: "block",
  //         margin: "20px auto",
  //         width: "25%"
  //       }
  //     },
  //     MuiAppBar: {
  //       root: {
  //         marginBottom: "35px"
  //       }
  //     },
  //     MuiTableContainer: {
  //       root: {
  //         display: "block",
  //         margin: "20px auto",
  //         width: "80%"
  //       }
  //     }
  //   }
});

export default theme;
