# Frontend para la prueba de seleccion fullstack de Tactech

## Modo de instalación.

- Clonar este repo.
- Luego en una terminal ejecutar

```bash
  $ cd frontend
  $ npm install
```

- Luego para ejecutarlo en local

```bash
  $ npm run dev 
```

Para que se muestren los personajes se debe estar ejecutando el backend que está en el root de este repo.


## Herramientas utilizadas:

- [React v16.13.1](https://reactjs.org/)
- [NextJs v9.5.1](https://nextjs.org/)
- [Redux v4.0.5](https://redux.js.org/)
- [Material-UI v10.0.28](https://material-ui.com/)
- [Emotion v10.0.28](https://emotion.sh/docs/introduction)

Gracias por la oportunidad!

Enjoy it 🚀😎

Antonio Rodríguez
FullStack Developer.
