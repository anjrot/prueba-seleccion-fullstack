import characterReducer from "./characterReducer";
import { combineReducers } from "redux";

const rootReducer = combineReducers({
  characters: characterReducer
});

export default rootReducer;
