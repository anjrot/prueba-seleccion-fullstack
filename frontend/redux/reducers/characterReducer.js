import { GET_CHARACTERS } from "../actions/charactersAction";

const initialState = {};

const characterReducer = (state = initialState, action) => {
  switch (action.type) {
    case GET_CHARACTERS:
      return { ...state, payload: action.payload };

    default:
      return state;
  }
};

export default characterReducer;
