import axios from "axios";

export const GET_CHARACTERS = "GET_CHARACTERS";

export const getCharacters = query => async dispatch => {
  const page = query?.page || "";
  const name = query?.name || "";
  try {
    const fetchCharacters = await axios.get(`http://localhost:4000/api/characters?page=${page}&name=${name}`);
    dispatch({
      type: GET_CHARACTERS,
      payload: fetchCharacters.data
    });
  } catch (error) {
    console.error("error :>> ", error);
  }
};
